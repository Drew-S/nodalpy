#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""The scene for the editor view.

Author: Drew Sommer
Version: 0.0.5
License: MIT
"""
from PyQt5.QtCore import pyqtSlot, QPoint
from PyQt5.QtGui import QPen, QColor
from PyQt5.QtWidgets import QGraphicsScene, QGraphicsProxyWidget, QGraphicsSceneMouseEvent

from NodalPy.parts.terminal import Wire


class GraphEditorScene(QGraphicsScene):
  """The QGraphicsScene for the NodeEditor view widget."""

  def __init__(self, width=4000, height=4000, graphEditor=None, parent=None):
    """Create a new editor scene.
    
    Args:
      width (int): The width of the scene (default: 4000)
      height (int): The height of the scene (default: 4000)
      graphEditor (GraphEditor, optional): The parent graphics view
      parent (QObject, optional): The parent widget
    """
    super().__init__(parent)
    self.currentLine = None
    self.currentLineStartPos = None
    self.fromTerm = None
    self.toTerm = None
    self.fromTermIsIn = False
    self.graphEditor = graphEditor
    self.setSceneRect(-width/2, -height/2, width, height)

  def __mapGlobalToScene(self, pos):
    """Maps a global point to a scene point.

    Used for mapping a terminal global center point to the
      corresponding scene terminal.
    
    Args:
      pos (QPoint): The global point
    
    Returns:
      QPoint: The scene point
    """
    if self.graphEditor != None:
      return self.graphEditor.mapToScene(self.graphEditor.mapFromGlobal(pos))

    return QPoint(0, 0)

  def mouseReleaseEvent(self, event):
    """Overloaded QGraphicsScene function for managing mouse release.

    Used for when dragging a wire to drop the wire (if not on a terminal) or
      to connect the wire to a dropped terminal.
    
    Args:
      event (QGraphicsSceneMouseEvent): The mouse event
    """
    if self.currentLine != None and self.fromTerm != None:
      dropped = False
      term = None
      for obj in self.items(event.scenePos()):
        if type(obj) == QGraphicsProxyWidget and callable(getattr(obj.widget(), '_droppedLine', None)):
          term = obj.widget()._droppedLine(event.scenePos())
          dropped = term != None
          break

      if dropped and self.fromTerm.term != term.term:
        self.fromTerm._addWireConnection(self.currentLine, term)
        term._addWireConnection(self.currentLine, self.fromTerm)
        termPos = self.__mapGlobalToScene(term.globalCenter())
        fromTermPos = self.__mapGlobalToScene(self.fromTerm.globalCenter())
        self.currentLine.startTerm = self.fromTerm
        self.currentLine.endTerm = term
        self.currentLine.setPath(self.currentLine.getPath())
        if (
          self.fromTerm.term == term.IN_TERM
          and term.parentWidget() != None
          and term.parentWidget().parentWidget()._isCyclic(None, [], [])
        ) \
        or (
          self.fromTerm.parentWidget() != None
          and self.fromTerm.parentWidget().parentWidget()._isCyclic(None, [], [])
        ):
          term._removeLastWireConnection()
          self.fromTerm._removeLastWireConnection()
          self.removeItem(self.currentLine)

      else:
        self.removeItem(self.currentLine)
        if self.fromTermIsIn:
          # con = self.toTerm.wireConnections[0]
          for t in self.fromTerm.wireConnections:
            if t['term'] == self.toTerm:
              self.fromTerm.wireConnections.remove(t)
              break

          # self.toTerm.wireConnections.pop(0)
          self.fromTermIsIn = False

      self.currentLine = None
      self.fromTerm = None
      self.toTerm = None

  @pyqtSlot(object) # object is a Terminal
  def _beginConnectTerminal(self, term):
    """Begin drawing a wire to connect terminals.
    
    Args:
      term (Terminal): The terminal the wire starts from
    """
    self.fromTerm = term
    if self.currentLine == None:
      if term.term == term.OUT_TERM:
        self.currentLineStartPos = self.__mapGlobalToScene(self.fromTerm.globalCenter())
        pen = QPen()
        pen.setColor(QColor(32, 32, 32))
        pen.setWidth(2.5)
        self.currentLine = Wire(term, None, self.graphEditor)
        self.addItem(self.currentLine)
        # self.currentLine = self.addLine(self.currentLineStartPos.x(), self.currentLineStartPos.y(), self.currentLineStartPos.x(), self.currentLineStartPos.y(), pen)
        self.currentLine.setZValue(-1)
        self.fromTermIsIn = False

      elif len(term.wireConnections) > 0:
        con = term.wireConnections.pop(0)
        self.fromTerm = con['term']
        self.toTerm = term
        self.currentLineStartPos = self.__mapGlobalToScene(self.fromTerm.globalCenter())
        self.currentLine = con['line']
        self.fromTermIsIn = True
        term.disconnectedWire()

      else:
        self.currentLineStartPos = self.__mapGlobalToScene(self.fromTerm.globalCenter())
        self.currentLine = Wire(term, None, self.graphEditor, True)
        self.addItem(self.currentLine)
        # self.currentLine = self.addLine(self.currentLineStartPos.x(), self.currentLineStartPos.y(), self.currentLineStartPos.x(), self.currentLineStartPos.y(), pen)
        self.currentLine.setZValue(-1)
        self.fromTermIsIn = False
        
  def mouseMoveEvent(self, event):
    """Overloaded QGraphicsScene function to update the drawing of a wire.
    
    Arguments:
      event (QGraphicsSceneMouseEvent): The mouse event
    """
    if self.currentLine != None:
      self.currentLine.setPath(self.currentLine.getPath(event.scenePos()))

    if isinstance(event, QGraphicsSceneMouseEvent): # Needed for eventhelper in testing background
      super().mouseMoveEvent(event) # Needed to move nodes