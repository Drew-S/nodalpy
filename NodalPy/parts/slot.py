#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""Create slots on a graphNode.

Author: Drew Sommer
Version: 0.0.2r2
License: MIT
"""
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QLabel

from NodalPy.parts.terminal import Terminal, DeadTerminal

class GraphNodeSlot(QWidget):
  """Create a slot for connecting terminals in the node"""

  inputDataChange = pyqtSignal(object)
  """Terminal input data change signal."""

  outputDataChange = pyqtSignal(object)
  """Terminal output data change signal."""

  def __init__(self, input=None, output=None, widget=None, parent=None):
    """Create a slot in a node, with or without terminals or nodes.
    
    Args:
      input (dict|None): Dictionary defining the input terminal details (default: None)
      output (dict|None): Dictionary defining the output terminal details (default: None)
      widget (QWidget|None): The widget to appear in the middle of a slot (default: None)
      parent (QObject|None): The holding parent (default: None)
    """
    if input != None and not isinstance(input, dict):
      raise TypeError('Unexpected type, expected None or dict got: ' + str(type(input)))

    if output != None and not isinstance(output, dict):
      raise TypeError('Unexpected type, expected None or dict got: ' + str(type(output)))

    if widget != None and not isinstance(widget, QWidget):
      raise TypeError('Unexpected type, expected None or QWidget got: ' + str(type(widget)))
      
    super().__init__(parent)
    self.scene = None
    self.output = None
    self.input = None
    self.widget = QLabel('')

    layout = QHBoxLayout()

    if input != None and type(input) == dict:
      inLimit = False
      basisType = 0
      basis = 0
      if 'type' in input.keys():
        basisType = input['type']

      if 'basis' in input.keys():
        basis = input['basis']

      if 'limit' in input.keys():
        inLimit = input['limit']

      self.input = Terminal(basisType, basis, Terminal.IN_TERM, inLimit=inLimit)
      self.input.dataChanged.connect(self.onInputDataChanged)
      layout.addWidget(self.input)

    else:
      layout.addWidget(DeadTerminal())

    if isinstance(widget, QWidget):
      self.widget = widget

    layout.addWidget(self.widget)

    if output != None and type(output) == dict:
      basisType = 0
      basis = 0
      if 'type' in output.keys():
        basisType = output['type']

      if 'basis' in output.keys():
        basis = output['basis']

      self.output = Terminal(basisType, basis, Terminal.OUT_TERM)
      self.output.dataChanged.connect(self.onOutputDataChanged)
      layout.addWidget(self.output)

    else:
      layout.addWidget(DeadTerminal())

    self.setLayout(layout)

  @pyqtSlot(object)
  def onInputDataChanged(self, data):
    """Used to propagate the input terminal dataChanged signal up.
    
    Args:
      data (object): The data that was changed
    """
    self.inputDataChange.emit(data)

  @pyqtSlot(object)
  def onOutputDataChanged(self, data):
    """Used to propagate the output terminal dataChanged signal up.
    
    Args:
      data (object): The data that was changed
    """
    self.outputDataChange.emit(data)

  def getInputData(self):
    """Get the input terminals data
    
    Returns:
      object | None: The data stored by the input terminal or None
    """
    if self.input == None:
      return None

    return self.input.getData()

  def setOutputData(self, data):
    """Set the output terminals data.
    
    Args:
      data (object): The data to be set on the output terminal
    """
    if self.output == None:
      return

    self.output.setData(data)