#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""Some helpful background rects for widgets.

Author: Drew Sommer
Version: 0.0.0
License: MIT
"""
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QGraphicsRectItem

class Background(QGraphicsRectItem):
  """GraphNode background."""
  
  def __init__(self, parent=None):
    """Create a new background rect.
    
    Args:
      parent (QObject): The parent widget
    """
    super().__init__(parent)
    self.mousePos = None
    self.isDragging = False
    self.parent = parent

    self.setRect(32, 15, 0, 0)

  def mousePressEvent(self, event):
    """Overloaded QGraphicsRectItem event handler.

    Starts the drag move handler.
    
    Args:
      event (QGraphicsSceneMouseEvent): The mouse event
    """
    self.isDragging = True
    self.mousePos = event.pos()

  def mouseReleaseEvent(self, event):
    """Overloaded QGraphicsRectItem event handler.

    Stops the drag move handler.
    
    Args:
      event (QGraphicsSceneMouseEvent): The mouse event
    """
    self.isDragging = False

  def mouseMoveEvent(self, event):
    """Overloaded QGraphicsRectItem event handler.

    Moves the widget
    
    Args:
      event (QGraphicsSceneMouseEvent): The mouse event
    """
    if self.isDragging:
      self.parent.setPos(event.scenePos() - self.mousePos)

  def paint(self, painter, *_):
    """Handles the background painting.
    
    Args:
      painter (QPainter): Used to control the drawing of the widget
    """
    painter.setBrush(QColor(32, 46, 56, 180))
    pen = self.pen()
    pen.setColor(QColor(0, 0, 0, 180))
    pen.setWidth(2)
    painter.setPen(pen)
    painter.drawRoundedRect(self.rect(), 12.0, 12.0)
