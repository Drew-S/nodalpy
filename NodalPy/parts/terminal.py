#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""Create terminal in a slot.

Author: Drew Sommer
Version: 0.1.1
License: MIT
"""
import re

from PyQt5.QtCore import Qt, QPoint, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QColor, QPainter, QPainterPath, QPen
from PyQt5.QtWidgets import QWidget, QGraphicsPathItem

class Wire(QGraphicsPathItem):
  """Display line for between two terminals."""

  def __init__(self, startTerm, endTerm, graphEditor, reverse=False, easeDiff=0.75, parent=None):
    """Create a wire to connect two terminals.
    
    Args:
      startTerm (Terminal): The starting terminal
      endTerm (Terminal): The ending terminal (instantiated as None for drawing to mouse position)
      graphEditor (GraphEditor): The graph editor the wire exists in
      reverse (bool, optional): Reverse the drawing, for when an in term is drawn from
      easeDiff (float, optional): The control point offset percentage between the two terminals (default: 0.75), (0.0 is the same as a straight line)
      parent (QObject, optional): The parent widget
    """
    is_graph_editor = False
    for i in graphEditor.__class__.__bases__:
      if 'GraphEditor' in str(i):
        is_graph_editor = True
        break
    
    if 'GraphEditor' in str(graphEditor.__class__):
      is_graph_editor = True
        # print('true')
    # if '<class \'NodalPy.graphEditor.GraphEditor\'>' in graphEditor.__class__.__bases__:
    if not is_graph_editor:
      raise TypeError('Unexpected type, expected GraphEditor not: ' + str(type(graphEditor)))

    if not isinstance(startTerm, Terminal):
      raise TypeError('Unexpected type, expected Terminal not: ' + str(type(startTerm)))
      
    if endTerm != None and not isinstance(endTerm, Terminal):
      raise TypeError('Unexpected type, expected Terminal not: ' + str(type(endTerm)))

    if not isinstance(reverse, bool):
      raise TypeError('Unexpected type, expected bool not: ' + str(type(reverse)))

    if not isinstance(easeDiff, float):
      raise TypeError('Unexpected type, expected float not: ' + str(type(reverse)))

    super().__init__(parent)
    self.graphEditor = graphEditor
    self.startTerm = startTerm
    self.endTerm = endTerm
    self.reverse = reverse
    self.easeDiff = easeDiff

    pen = QPen()
    pen.setColor(QColor(32, 32, 32))
    pen.setWidth(2.5)

    self.setPen(pen)

  def __mapGlobalToScene(self, pos):
    """Map a global point to the scene.

    Used to map the endTerm center pos to the scene.
    
    Args:
      pos (QPoint): The global position
    
    Returns:
      QPoint: The scene position
    """
    return self.graphEditor.mapToScene(self.graphEditor.mapFromGlobal(pos))

  def getPath(self, pos=None):
    """Calculate the path from between the terminals.

    Gets the path from the startTerm to the endTerm,
      or the startTerm to the mouse position.
    
    Args:
      pos (QPoint, optional): The mouse position, overrides final drawing position of path
    """
    start = self.__mapGlobalToScene(self.startTerm.globalCenter())
    if pos != None:
      end = pos

    elif self.endTerm != None:
      end = self.__mapGlobalToScene(self.endTerm.globalCenter())

    else:
      return None

    midDiff = abs(end.x() - start.x()) * self.easeDiff
    if self.reverse:
      control1 = QPoint(start.x() - midDiff, start.y())
      control2 = QPoint(end.x() + midDiff, end.y())

    else:
      control1 = QPoint(start.x() + midDiff, start.y())
      control2 = QPoint(end.x() - midDiff, end.y())

    path = QPainterPath(start)
    path.cubicTo(control1, control2, end)
    return path

  def delete(self):
    self.graphEditor.scene.removeItem(self)


class Terminal(QWidget):
  """Terminal for connecting wires between nodes, data flow connections."""
  TYPE_ANY = 0
  TYPE_INT = 1
  TYPE_FLOAT = 2
  TYPE_STRING = 3

  BASIS_BASE = 0
  BASIS_VECTOR = 1
  BASIS_VECTOR2 = 2
  BASIS_VECTOR3 = 3
  BASIS_SAMPLER2D = 4
  BASIS_SAMPLER3D = 5

  IN_TERM = 0
  OUT_TERM = 1

  TYPE_COLORS = [QColor(169, 169, 169), QColor(55, 49, 206), QColor(237, 154, 0), QColor(116, 255, 0)]

  dataChanged = pyqtSignal(object)
  """Signal for when the input terminal has its data updated.
  
  Returns:
    object: The data that was changed
  """

  terminalClicked = pyqtSignal(object)
  """Signal for when the terminal is clicked, begin wire drawing.
  
  Returns:
    Terminal: self
  """

  center = QPoint(12, 12)
  """The terminals center position."""

  def __init__(self, basisType=0, basis=0, term=0, inLimit=False, parent=None):
    """Create a new terminal for data connections.
    
    Args:
      basisType (int, optional): The type of variable (default: TYPE_ANY)
      basis (int, optional): The expected shape of the variable (default: BASIS_BASE)
      term (int, optional): The type of terminal (default: IN_TERM)
      inLimit (bool, optional): Whether or not to limit an input terminal to one wire (default, False)
      parent (QObject, optional): The parent widget (default: None)
    """
    if not isinstance(basisType, int):
      raise TypeError('Unexpected type, expected int not: ' + str(type(basisType)))

    if not isinstance(basis, int):
      raise TypeError('Unexpected type, expected int not: ' + str(type(basis)))

    if not isinstance(term, int):
      raise TypeError('Unexpected type, expected int not: ' + str(type(term)))

    if not isinstance(inLimit, bool):
      raise TypeError('Unexpected type, expected bool not: ' + str(type(inLimit)))

    super().__init__(parent)
    # self.scene = None
    # self.scenePos = None

    self.wireConnections = []

    self.term = int(term) % 2
    self.basisType = int(basisType) % 4
    self.basis = int(basis) % 6
    self.inLimit = inLimit

    self.data = None

    self.setFixedSize(24.0, 24.0)
    self.__setToolTip()

  def __setToolTip(self):
    """Sets the tooltip of the terminal

    examples:
      any
      Vector<float>
      Vector2<int>
      Vector3<str>
      Sampler2D[int]
      Sampler3D[float]
    """
    pre = ''
    post = ''
    mid = ''

    if self.basis == self.BASIS_VECTOR:
      pre = 'Vector<'
      post = '>'

    elif self.basis == self.BASIS_VECTOR2:
      pre = 'Vector2<'
      post = '>'
      
    elif self.basis == self.BASIS_VECTOR3:
      pre = 'Vector3<'
      post = '>'

    elif self.basis == self.BASIS_SAMPLER2D:
      pre = 'Sampler2D['
      post = ']'

    elif self.basis == self.BASIS_SAMPLER3D:
      pre = 'Sampler3D['
      post = ']'

    if self.basisType == self.TYPE_ANY:
      mid = 'any'

    elif self.basisType == self.TYPE_INT:
      mid = 'int'

    elif self.basisType == self.TYPE_FLOAT:
      mid = 'float'

    elif self.basisType == self.TYPE_STRING:
      mid = 'string'

    self.setToolTip(pre + mid + post)

  def getColor(self):
    """Get the color of the node. Color is defined by the TYPE_* enum
    
    Returns:
      QColor: The color of the node
    """

    return self.TYPE_COLORS[self.basisType]

  def globalCenter(self):
    """Get the global center position of the terminal.
    
    Returns:
      QPoint: The global center point
    """
    return self.mapToGlobal(self.center)

  def paintEvent(self, _):
    """Overloaded function from QWidget, used to draw terminal shapes."""
    painter = QPainter(self)
    pen = painter.pen()
    brush = painter.brush()
    brush.setColor(QColor(103, 105, 111))
    brush.setStyle(Qt.SolidPattern)
    pen.setColor(self.getColor())
    pen.setWidth(2.5)
    painter.setPen(pen)
    painter.setBrush(brush)
    if self.basis == self.BASIS_BASE:
      painter.drawEllipse(6.0, 6.0, 12.0, 12.0)

    elif self.basis > self.BASIS_BASE and self.basis < self.BASIS_SAMPLER2D:
      painter.rotate(45)
      painter.drawRect(12.0, -6.0, 12.0, 12.0)

    else:
      painter.drawRect(6.0, 6.0, 12.0, 12.0)

  def disconnectedWire(self):
    """Resets the data when a wire is disconnected."""
    self.data = None
    self.getData()
    self.dataChanged.emit(self.data)

  def mousePressEvent(self, _):
    """Overloaded function, clicking on terminal starts drawing a wire."""
    self.terminalClicked.emit(self)

  @pyqtSlot(object)
  def _onInputChange(self, data):
    """When data from another node connected to this terminal is updated.
    
    Args:
      data (object): The updated data from the other node
    """
    self.getData()
    self.dataChanged.emit(self.data)

  def _addWireConnection(self, line, term):
    """Adds a wire connection between two terminals.
    
    Args:
      line (QGraphicsLineItem): The line connecting two terminals
      term (Terminal): The other terminal this one is additionally connected to
    """
    self.wireConnections.append({
      'line': line,
      'term': term
    })
    if self.term == self.IN_TERM:
      if self.inLimit and len(self.wireConnections) > 1:
        wire = self.wireConnections.pop(0)
        for i in range(len(wire['term'].wireConnections)):
          if wire['term'].wireConnections[i]['term'] == self:
            wire['term'].wireConnections.pop(i)
            break

        wire['line'].delete()

      term.dataChanged.connect(self._onInputChange)
      self.getData()
      self.dataChanged.emit(self.data)

  def _removeLastWireConnection(self):
    """Used to remove the last made connection.

    After two wires are connected, the scene checks for cycles,
      if there is a cycle, the wires are disconnected.
    """
    if len(self.wireConnections) > 0:
      wire = self.wireConnections.pop(len(self.wireConnections) - 1)
      if self.term == self.IN_TERM:
        wire['term'].dataChanged.disconnect(self._onInputChange)

  def _removeWire(self, index):
    """Remove a wire from the list of connections by index

    Args:
      index (int): The index of the wire to remove
    """
    if not isinstance(index, int):
      raise TypeError('Unexpected type, expected Integer not: ' + str(type(index)))

    if index < 0 or index >= len(self.wireConnections):
      raise IndexError('Index out of range')

    wire = self.wireConnections.pop(index)
    if self.term == self.IN_TERM:
      wire['term'].dataChanged.disconnect(self._onInputChange)

    for i in range(len(wire['term'].wireConnections)):
      if wire['term'].wireConnections[i]['term'] == self:
        wire['term']._removeWire(i)
        break

    if wire['line'] != None:
      wire['line'].delete()
    
  def getData(self):
    """Returns the data that the terminal holds.
    
    If the terminal is an output terminal, it simply returns `data`.
    If the terminal is an input terminal, it retrieves all the data from
      connected terminals and returns that.

    Returns:
      object: The data to be returned
    """

    if self.term == self.IN_TERM and len(self.wireConnections) > 0:
      self.data = []
      if len(self.wireConnections) == 1:
        self.data = self.wireConnections[0]['term'].getData()
      else:
        for wire in self.wireConnections:
          self.data.append(wire['term'].getData())

    return self.data

  def setData(self, data):
    """Sets the terminals data, and emits dataChanged signal.

    After setting the data, the signal informs the node and any connected wires
      listening for data changes.
    
    Args:
      data (object): The new data to set
    """
    self.data = data
    self.dataChanged.emit(self.data)

  def __eq__(self, t):
    if not isinstance(t, Terminal):
      return False

    if (
      self.term == t.term
      and self.basisType == t.basisType
      and self.basis == t.basis
    ):
      return True
    
    return False


class DeadTerminal(Terminal):
  """Special blank terminal."""

  def __init__(self, parent=None):
    """Creates a blank terminal."""
    super().__init__(0, 0, 0, False, parent)

  def paintEvent(self, _):
    """Overloaded QWidget function that does not display a terminal."""
    pass
