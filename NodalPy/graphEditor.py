#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""Primary Graph editor file, used for creating a editor area.

Author: Drew Sommer
Version: 0.0.0r1
License: MIT
"""

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor, QPainter, QBrush
from PyQt5.QtWidgets import QGraphicsView

from NodalPy.parts.scene import GraphEditorScene
from NodalPy.graphNode import GraphNode


class GraphEditor(QGraphicsView):
  """The main widget for housing the node editor."""


  def __init__(self, width=4000, height=4000, parent=None):
    """Create a graph editor area.
    
    Args:
      width (int): The width of the QGraphicsScene (default: 4000)
      height (int): The height of the QGraphicsScene (default: 4000)
      parent (QObject, optional) -- [description] (default: None)
    """
    if not isinstance(width, int):
      raise TypeError('Expected integer not: ' + str(width))

    elif width < 0:
      raise ValueError('Out of range, positive integers only')

    if not isinstance(height, int):
      raise TypeError('Expected integer not: ' + str(height))

    elif height < 0:
      raise ValueError('Out of range, positive integers only')


    super().__init__(parent)
    self.nodes = []
    self.scaleValue = 1.0

    self.scene = GraphEditorScene(width, height, self)
    self.setScene(self.scene)

    brush = QBrush(QColor(55, 57, 61), Qt.CrossPattern)

    self.setStyleSheet('QGraphicsView { background: rgb(103, 105, 111); }')
    self.setBackgroundBrush(brush)

    self.setDragMode(QGraphicsView.ScrollHandDrag)
    self.setRenderHints(QPainter.Antialiasing)

  def addNode(self, item):
    """Add a new node to the editor.

    TODO:
      add optional position placement
    
    Args:
      item (GraphNode): The node to be placed in the editor
    """
    if not isinstance(item, GraphNode):
      raise TypeError('Expected GraphNode not: ' + str(item))
      
    item.graphEditor = self
    self.nodes.append(item)
    item._sceneAdded(self.scene)

  def wheelEvent(self, event):
    """Overloaded QGraphicsView function.

    Manages using the mouse scroll wheel for zooming in and out.
    
    Args:
      event (QGraphicsSceneWheelEvent): Mouse wheel even
    """
    self.scaleValue += 0.1 if event.angleDelta().y() > 0 else -0.1
    if self.scaleValue < 0.5:
      self.scaleValue = 0.5
    
    elif self.scaleValue > 5.0:
      self.scaleValue = 5.0

    self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
    self.resetTransform()
    self.scale(self.scaleValue, self.scaleValue)
    event.ignore()