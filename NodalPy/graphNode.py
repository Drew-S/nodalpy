#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""Primary Graph Node file, used for creating nodes.

Author: Drew Sommer
Version: 0.1.0
License: MIT
"""


from math import sqrt

from PyQt5.QtCore import QPointF, QPoint, Qt, QSize, pyqtSlot, pyqtSignal, QTimer
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QVBoxLayout, QLabel, QGraphicsItem, QLineEdit, QPushButton

from NodalPy.parts.nodeBackground import Background
from NodalPy.parts.slot import GraphNodeSlot


class GraphNode(QGraphicsItem):
  """Primary node used to add custom editor node.

  Inherit this class to create custom nodes.

  Example:
  
    class MyNode(GraphNode):
      # Displays the input list length.

      def __init__(self, parent=None):
        super().__init__('MyNode Title', parent)

        self.label = QLabel()

        slot = self.addSlot({
          'type': Terminal.TYPE_ANY,
          'basis': Terminal.BASIS_BASE
        }, None, self.label)
        slot.inputDataChange.connect(self.onInputChange)

      @pyqtSlot(list)
      def onInputChange(self, input):
        self.label.setText("Input list is " + str(len(input)) + " long.")
  """

  def __init__(self, title='GraphNode', close=True, parent=None):
    """Create a graph node.
    
    Args:
      title (str): The title shown in the node header (default: 'GraphNode')
      close (bool, optional): Whether or not to include the close button (default: True)
      parent (QObject, optional): The parent container (default: None)
    """
    if not isinstance(title, str):
      raise TypeError('title should be a String, not: ' + str(type(title)))

    if not isinstance(close, bool):
      raise TypeError('close should be a Boolean, not: ' + str(type(close)))

    super().__init__(parent)
    self.graphEditor = None
    self.proxy = None
    self.title = title
    self.background = Background(self)
    self.__preWidth = 0

    self.widget = GraphNodeProxy(title, self, close)

  def _sceneAdded(self, scene):
    """Pass the scene reference.
    
    Args:
      scene (GraphEditorScene): The scene used in other references
    """
    scene.addItem(self.background)
    # self.widget._setScene(scene)
    self.proxy = scene.addWidget(self.widget)
    for slot in self.widget.slots:
      if slot.input != None:
        slot.input.terminalClicked.connect(scene._beginConnectTerminal)

      if slot.output != None:
        slot.output.terminalClicked.connect(scene._beginConnectTerminal)
    
    self._updateSize()

  def setPos(self, pos):
    """Update the position of the node.

    Overloaded function from QGraphicsItem
    
    Args:
      pos (QPoint): The updated position
    """
    if not isinstance(pos, QPointF):
      raise TypeError('pos needs to be a QPointF, not: ' + str(type(pos)))

    super().setPos(pos)
    self.background.setPos(pos)
    self.proxy.setPos(pos)
    self.__updateLinesPos(pos)

  def addSlot(self, input=None, output=None, widget=None):
    """Add a new slot item.
    
    Args:
      input (dict, optional): Input terminal details (default: None)
      output (dict, optional): Output terminal details (default: None)
      widget (QWidget, optional): Custom widget for the slot (default: None)
    
    Returns:
      GraphNodeSlot: A reference to the newly created slot
    """
    slot = self.widget.addSlot(input, output, widget)
    self._updateSize()
    return slot

  def _updateSize(self):
    """Updates the size of widget when widgets scale."""
    def up():
      self.widget.adjustSize()
      widgetRect = self.widget.rect()
      # diff = self.__preWidth - widgetRect.width()
      # off = 0
      # if diff < 0:
      #   off = 8
      # self.__preWidth = widgetRect.width()
      rect = self.background.rect()
      self.background.setRect(rect.x(), rect.y(), widgetRect.width() - 64, widgetRect.height() - 5)

    timer = QTimer()
    timer.singleShot(5, up)

  def addWidget(self, widget):
    """Add a widget slot.

    Simple alias for `GraphNode.addSlot(None, None, widget)`
    
    Arguments:
      widget (QWidget): Custom widget added in a slot
    """
    self.addSlot(None, None, widget)

  def getSlots(self):
    """Get the list of slots with terminals.
    
    Returns:
      list: The list of slots with terminals
    """
    return self.widget.slots

  def _mapGlobalToScene(self, pos):
    """Maps a global point to a scene point.

    Used for mapping a terminal global center point to the
      corresponding scene terminal.
    
    Args:
      pos (QPoint): The global point
    
    Returns:
      QPoint: The scene point
    """
    if not isinstance(pos, QPoint):
      raise TypeError('Unexpected type, expected QPoint not: ' + str(type(pos)))

    return self.graphEditor.mapToScene(self.graphEditor.mapFromGlobal(pos))

  def __updateLinesPos(self, pos):
    """Update the wires positions.

    Updates wire connection positions when the widget is dragged.
    
    Args:
      pos (QPoint): Update based on this nodes new spot
    """
    for slot in self.widget.slots:
      if slot.input != None:
        for wire in slot.input.wireConnections:
          f = self._mapGlobalToScene(slot.input.globalCenter())
          # f = slot.input._getScenePos()
          t = self._mapGlobalToScene(wire['term'].globalCenter())
          # t = wire['term']._getScenePos()
          wire['line'].setPath(wire['line'].getPath())

      if slot.output != None:
        for wire in slot.output.wireConnections:
          f = self._mapGlobalToScene(slot.output.globalCenter())
          # f = slot.output._getScenePos()
          t = self._mapGlobalToScene(wire['term'].globalCenter())
          # t = wire['term']._getScenePos()
          wire['line'].setPath(wire['line'].getPath())


class GraphNodeProxy(QWidget):
  """Proxy Node where actual QWidgets reside"""

  closed = pyqtSignal()

  def __init__(self, title='GraphNode', control=None, close=True, parent=None):
    """Create a proxy widget.

    Holds "regular" QWidget objects for displaying in the QGraphicsScene.
    
    Args:
      title (str): The title that is displayed in blank slot as QLabel (default: 'GraphNode')
      control (GraphNode, optional): The parent node that creates the editor (default: None)
      close (bool, optional): Whether or not to include the close button (default: True)
      parent (QObject, optional): The parent widget that manages this widget, QGraphicsProxyWidget (default: None)
    """
    if not isinstance(title, str):
      raise TypeError('title should be a String, not: ' + str(type(title)))

    if not isinstance(close, bool):
      raise TypeError('close should be a Boolean, not: ' + str(type(close)))
    
    if control != None and not isinstance(control, GraphNode):
      raise TypeError('Unexpected type, expected GraphNode or None not: ' + str(type(control)))

    super().__init__(parent)
    self.scene = None
    self.control = control
    self.slots = []

    self.setObjectName('GraphNodeProxy')

    self.header = QWidget()
    header_layout = QHBoxLayout()
    self.header.setLayout(header_layout)
    self.header_title = QLabel(title)
    self.header_close = None
    header_layout.addWidget(self.header_title)
    header_layout.addStretch()
    if close:
      self.header_close = QPushButton('x', self)
      self.header_close.pressed.connect(self.close)
      self.header_close.setFixedSize(24, 24)
      header_layout.addWidget(self.header_close)

    layout = QVBoxLayout()
    self.setLayout(layout)
    self.addSlot(None, None, self.header)
    self.setStyleSheet("""
      QWidget#GraphNodeProxy {
        background: rgba(0, 0, 0, 0);
      }
      QLabel {
        color: rgba(255, 255, 255, 255);
      }
      QPushButton {
        background: none;
        border-radius: 6px;
        border: none;
        color: rgba(255, 255, 255, 255);
        font-size: 16px;
      }
      QPushButton:hover {
        background: rgba(117, 117, 117, 117);
      }
    """)

  def close(self):
    """closes the widget
    
    removes all wires and removes widget from memory."""
    for slot in self.slots:
      if slot.input != None:
        while len(slot.input.wireConnections):
          slot.input._removeWire(0)
      
      if slot.output != None:
        while len(slot.output.wireConnections):
          slot.output._removeWire(0)

    self.closed.emit()
    self.control.graphEditor.nodes.remove(self.control)
    self.control.graphEditor.scene.removeItem(self.control.background)
    self.control.graphEditor.scene.removeItem(self.control.proxy)

  def _droppedLine(self, pos):
    """Check to see if wire dropped on terminal.

    Checks to see if the wire is being dropped onto a terminal point.

    TODO: Change name to be more descriptive
    
    Args:
      pos (QPoint): The mouses position
    
    Returns:
      Terminal|None: The terminal under the mouse
    """
    for slot in self.slots:
      if slot.input != None:
        p = self.control._mapGlobalToScene(slot.input.globalCenter()) - pos
        # p = slot.input._getScenePos() - pos
        if sqrt(p.x()**2 + p.y()**2) < 25:
          return slot.input

      if slot.output != None:
        p = self.control._mapGlobalToScene(slot.output.globalCenter()) - pos
        # p = slot.output._getScenePos() - pos
        if sqrt(p.x()**2 + p.y()**2) < 25:
          return slot.output

  @pyqtSlot(object)
  def _updateSize(self, _):
    """Listens for data changes on terminals.

    If there is a data change the widget may need to be resized.

    Ignores the input data.
    """
    if self.control != None:
      self.control._updateSize()

  def addSlot(self, input=None, output=None, widget=None):
    """Adds a slot to the widget.
    
    Args:
      input (dict, optional): Input terminal details (default: None)
      output (dict, optional): Output terminal details (default: None)
      widget (QWidget, optional): Custom widget for slot (default: None)
    
    Returns:
      GraphNodeSlot: Reference to the added slot
    """
    slot = GraphNodeSlot(input, output, widget)
    slot.inputDataChange.connect(self._updateSize)
    slot.outputDataChange.connect(self._updateSize)
    self.slots.append(slot)
    self.layout().addWidget(slot)
    self.adjustSize()
    return slot

  def _isCyclic(self, node=None, next=[], visited=[]):
    """Checks to see if the nodes are in a cycle connection.

    Used to ensure cycles do not occur.
      Uses breadth-first search.
    
    Args:
      node (GraphNodeProxy, optional): The node that were hoping to not find (default: None)
      next (list): The list of nodes to check next
      visited (list): The list of visited nodes
    
    Returns:
      bool: Whether or not a cycle was found
    """
    if node == None:
      node = self

    elif self == node:
      return True

    else:
      visited.append(self)
    
    for slot in self.slots:
      if slot.output != None:
        for wire in slot.output.wireConnections:
          if wire['term'].parentWidget() != None:
            if wire['term'].parentWidget().parentWidget() not in visited:
              next.append(wire['term'].parentWidget().parentWidget())

            elif wire['term'].parentWidget().parentWidget() == node:
              return True

    if len(next) == 0:
      return False

    n = next.pop(0)
    return n._isCyclic(node, next, visited)