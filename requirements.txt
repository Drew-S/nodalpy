PyQt5==5.12.1
Sphinx==1.8.5
sphinx-rtd-theme==0.4.3
pytest==4.3.1
pytest-qt==3.2.2
pytest-cov==2.6.1