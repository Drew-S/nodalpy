from setuptools import setup, find_packages

with open('README.rst', 'r') as fh:
  long_description = fh.read()

setup(
  name = 'NodalPy',
  version = '0.1.1',
  description = 'Nodal Graph editor for PyQt5',
  long_description = long_description,
  long_description_content_type = 'text/x-rst',
  license = 'MIT',
  author = 'Drew Sommer',
  author_email = 'drwsommer@gmail.com',
  packages = find_packages(),
  install_requires = ['PyQt5'],
  classifiers = [
    'Programming Language :: Python :: 3',
    'License :: OSI Approved :: MIT License',
    'Operating System :: OS Independent'
  ]
)