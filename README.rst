NodalPy
=======
A Graph Node editor for PyQt5
-----------------------------

.. image:: https://gitlab.com/Drew-S/nodalpy/badges/master/pipeline.svg
  :alt: pipeline status
.. image:: https://gitlab.com/Drew-S/nodalpy/badges/master/coverage.svg
  :alt: coverage report

Version ``0.1.1``

Create a graph node editor in PyQt5. Easy to use, simply create a `GraphEditor` widget and
add node widgets to it.

.. image:: https://gitlab.com/Drew-S/nodalpy/raw/master/docs/preview.png
  :alt: preview

example::

  import sys

  from PyQt5.QtWidgets import QApplication

  from NodalPy import GraphEditor, GraphNode, Terminal

  input = { 'type': Terminal.TYPE_ANY, 'basis': Terminal.BASIS_VECTOR }
  output = { 'type': Terminal.TYPE_FLOAT, 'basis': Terminal.BASIS_SAMPLER2D }

  app = QApplication(sys.argv)

  G = GraphEditor()

  node1 = GraphNode('Node 1')
  node2 = GraphNode('Node 2')
  node1.addSlot(input, output)
  node2.addSlot(input, output)

  G.addNode(node1)
  G.addNode(node2)

  G.show()

  sys.exit(app.exec_())

For a more in depth example see ``docs/examples/example4nodes.py``