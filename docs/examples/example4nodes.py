import sys

from PyQt5.QtWidgets import QApplication, QLineEdit, QLabel, QSpinBox
from PyQt5.QtCore import pyqtSlot

from NodalPy import GraphEditor, GraphNode, Terminal

class A(GraphNode):

  def __init__(self, parent=None):
    super().__init__('Input', parent=parent)

    control = QSpinBox()
    control.setRange(1, 100)

    output = { 'type': Terminal.TYPE_FLOAT, 'basis': Terminal.BASIS_BASE }

    self.controlSlot = self.addSlot(None, output, control)
    self.controlSlot.setOutputData(1)
    control.valueChanged.connect(self.onControlUpdate)

  def onControlUpdate(self, val):
    self.controlSlot.setOutputData(float(val))

class B(GraphNode):

  def __init__(self, parent=None):
    super().__init__('Square', parent=parent)

    input = { 'type': Terminal.TYPE_FLOAT, 'basis': Terminal.BASIS_BASE, 'limit': True }
    output = { 'type': Terminal.TYPE_FLOAT, 'basis': Terminal.BASIS_BASE }

    self.info = QLabel()
    self.passSlot = self.addSlot(input, output, self.info)
    self.passSlot.inputDataChange.connect(self.onInputChange)

  def onInputChange(self, val):
    if val != None and isinstance(val, float):
      self.info.setText(str(val**2.0))
      self.passSlot.setOutputData(val**2.0)
      
    else:
      self.info.setText('')

class C(GraphNode):

  def __init__(self, parent=None):
    super().__init__('Half', parent=parent)

    input = { 'type': Terminal.TYPE_FLOAT, 'basis': Terminal.BASIS_BASE, 'limit': True }
    output = { 'type': Terminal.TYPE_FLOAT, 'basis': Terminal.BASIS_BASE }

    self.info = QLabel()
    self.passSlot = self.addSlot(input, output, self.info)
    self.passSlot.inputDataChange.connect(self.onInputChange)

  def onInputChange(self, val):
    if val != None and isinstance(val, float):
      self.info.setText(str(val/2.0))
      self.passSlot.setOutputData(val/2.0)
      
    else:
      self.info.setText('')

class D(GraphNode):

  def __init__(self, parent=None):
    super().__init__('Add', parent=parent)
    self.a = 0
    self.b = 0

    input = { 'type': Terminal.TYPE_FLOAT, 'basis': Terminal.BASIS_BASE, 'limit': True }

    self.info = QLabel('0')
    self.slotA = self.addSlot(input, widget=self.info)
    self.slotB = self.addSlot(input)
    self.slotA.inputDataChange.connect(self.onInputChangeA)
    self.slotB.inputDataChange.connect(self.onInputChangeB)

  def onInputChangeA(self, val):
    if val != None and isinstance(val, float):
      self.a = val

    else:
      self.a = 0
      
    self.info.setText(str(self.a + self.b))

  def onInputChangeB(self, val):
    if val != None and isinstance(val, float):
      self.b = val

    else:
      self.b = 0

    self.info.setText(str(self.a + self.b))

class CustomGraphEditor(GraphEditor):
  pass

app = QApplication(sys.argv)

G = CustomGraphEditor()
G.show()

G.addNode(A())
G.addNode(B())
G.addNode(C())
G.addNode(D())

sys.exit(app.exec_())
