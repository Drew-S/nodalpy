.. NodalPy documentation master file, created by
   sphinx-quickstart on Mon Mar 25 10:19:01 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../../README.rst

.. toctree::
  :maxdepth: 2

  NodalPy
  NodalPy.graphEditor
  NodalPy.graphNode
  NodalPy.terminal
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
