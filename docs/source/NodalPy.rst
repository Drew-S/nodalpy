NodalPy package
===============

Subpackages
-----------

.. toctree::

    NodalPy.parts

Submodules
----------

NodalPy.graphEditor module
--------------------------

.. automodule:: NodalPy.graphEditor
    :members:
    :undoc-members:
    :show-inheritance:

NodalPy.graphNode module
------------------------

.. automodule:: NodalPy.graphNode
    :members:
    :undoc-members:
    :show-inheritance:

NodalPy.parts.terminal module
-----------------------------

.. autoclass:: Terminal
    :members:
    :exclude-members: TYPE_ANY, TYPE_INT, TYPE_FLOAT, TYPE_STRING,
        BASIS_BASE, BASIS_VECTOR, BASIS_VECTOR2, BASIS_VECTOR3,
        BASIS_SAMPLER2D, BASIS_SAMPLER3D, IN_TERM, OUT_TERM, TYPE_COLORS,
        paintEvent, mousePressEvent
    :undoc-members:
    :show-inheritance:

    **Enums**:
        The type of value expected by a terminal

        ``TYPE_ANY``, ``TYPE_INT``, ``TYPE_FLOAT``, ``TYPE_STRING``


        The shape of the collection expected. `BASE` is just a variable, `VECTOR` is a list,
        `VECTOR2` is a 2 element listm `VECTOR3` is a 3 element list, `SAMPLER2D` is a 2
        dimensional list, `SAMPLER3D` is a 3 dimensional list

        ``BASIS_BASE``, ``BASIS_VECTOR``, ``BASIS_VECTOR2``, ``BASIS_VECTOR3``, ``BASIS_SAMPLER2D``, ``BASIS_SAMPLER3D``

        Whether the terminal is an input (left) terminal or an output (right) terminal

        ``IN_TERM``, ``OUT_TERM``

Module contents
---------------

.. automodule:: NodalPy
    :members:
    :undoc-members:
    :show-inheritance:
