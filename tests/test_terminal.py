import pytest

from PyQt5.QtCore import QPoint, Qt, QSize
from PyQt5.QtGui import QColor, QPainterPath

from NodalPy.parts.terminal import Terminal, Wire
from NodalPy.graphEditor import GraphEditor


class TestWire():
  """
  [x] init
  [x] methods
    [x] getPath
    [x] delete
  """

  #====================================================================
  #                               INIT
  #====================================================================

  def testInitDefault(self, qtbot):
    wire = Wire(Terminal(), Terminal(), GraphEditor())
    qtbot.addWidget(wire)
    assert(isinstance(wire.startTerm, Terminal))
    assert(isinstance(wire.endTerm, Terminal))
    assert(isinstance(wire.graphEditor, GraphEditor))
    assert(wire.reverse == False)
    assert(wire.easeDiff == 0.75)

  successesInit = [
    (True, 0.5),
    (False, 0.1),
    (True, 0.0)
  ]

  @pytest.mark.parametrize('reverse, ease', successesInit)
  def testInitSuccesses(self, qtbot, reverse, ease):
    wire = Wire(Terminal(), Terminal(), GraphEditor(), reverse, ease)
    qtbot.addWidget(wire)
    assert(wire.reverse == reverse)
    assert(wire.easeDiff == ease)

  def testInitFailureDefault(self, qtbot):
    with pytest.raises(TypeError, match='Unexpected type, expected Terminal'):
      wire = Wire(False, Terminal(), GraphEditor())

    with pytest.raises(TypeError, match='Unexpected type, expected Terminal'):
      wire = Wire(Terminal(), {}, GraphEditor())

    with pytest.raises(TypeError, match='Unexpected type, expected GraphEditor'):
      wire = Wire(Terminal(), Terminal(), 123)

  failuresInit = [
    (123, False),
    ('test', 'test'),
    (12.2, []),
    ((), ()),
    ([], {}),
    ({}, lambda x: x+1),
    (lambda x: x+1, 'test')
  ]

  @pytest.mark.parametrize('reverse, ease', failuresInit)
  def testInitFailures(self, qtbot, reverse, ease):
    with pytest.raises(TypeError, match='Unexpected type, expected bool'):
      wire = Wire(Terminal(), Terminal(), GraphEditor(), reverse=reverse)

    with pytest.raises(TypeError, match='Unexpected type, expected float'):
      wire = Wire(Terminal(), Terminal(), GraphEditor(), easeDiff=ease)

  #====================================================================
  #                             METHODS
  #====================================================================

  def testGetPath(self, qtbot):
    editor = GraphEditor()
    terminal1 = Terminal(term=Terminal.IN_TERM)
    terminal2 = Terminal(term=Terminal.OUT_TERM)
    qtbot.addWidget(terminal1)
    qtbot.addWidget(terminal2)
    terminal2.move(100, 100)

    wire = Wire(terminal1, terminal2, editor)
    qtbot.addWidget(wire)

    expectedPath = QPainterPath(QPoint(0, 0))
    expectedPath.cubicTo(QPoint(75, 0), QPoint(25, 100), QPoint(100, 100))
    assert(isinstance(wire.getPath(), QPainterPath))

  def testDelete(self, qtbot):
    editor = GraphEditor()
    wire = Wire(Terminal(), None, editor)
    qtbot.addWidget(editor)
    editor.scene.addItem(wire)
    assert(wire in editor.scene.items())
    wire.delete()
    assert(wire not in editor.scene.items())


class TestTerminal():
  """
  [x] init
    [x] basisType
    [x] basis
    [x] term
    [x] size
    [x] inLimit
  [x] methods
    [x] getColor
    [x] globalCenter
    [x] disconnectedWire
    [x] setData
      [x] signal
    [x] getData
      [x] without wire
      [x] with wire
        [x] 2 terminals
        [x] 3 terminals
        [x] 10 terminals
    [x] wire limit
    [x] _addWireConnection
    [x] _removeLastWireConnection
    [x] _removeWire
    [x] mousePressEvent
  """

  #====================================================================
  #                               INIT
  #====================================================================

  def testInitDefault(self, qtbot):
    terminal = Terminal()
    qtbot.addWidget(terminal)
    assert(terminal.basisType == Terminal.TYPE_ANY)
    assert(terminal.basis == Terminal.BASIS_BASE)
    assert(terminal.term == Terminal.IN_TERM)
    assert(terminal.inLimit == False)
    assert(terminal.rect().size() == QSize(24.0, 24.0))
    assert(terminal.toolTip() == 'any')

  successesInit = [
    (Terminal.TYPE_ANY, Terminal.BASIS_BASE, Terminal.IN_TERM, False, 'any'),
    (Terminal.TYPE_FLOAT, Terminal.BASIS_VECTOR, Terminal.OUT_TERM, True, 'Vector<float>'),
    (Terminal.TYPE_INT, Terminal.BASIS_VECTOR2, Terminal.IN_TERM, False, 'Vector2<int>'),
    (Terminal.TYPE_STRING, Terminal.BASIS_VECTOR3, Terminal.IN_TERM, False, 'Vector3<string>'),
    (Terminal.TYPE_ANY, Terminal.BASIS_SAMPLER2D, Terminal.IN_TERM, False, 'Sampler2D[any]'),
    (Terminal.TYPE_ANY, Terminal.BASIS_SAMPLER3D, Terminal.IN_TERM, False, 'Sampler3D[any]')
  ]

  @pytest.mark.parametrize('basisType, basis, term, inLimit, tooltip', successesInit)
  def testInitSuccesses(self, qtbot, basisType, basis, term, inLimit, tooltip):
    terminal = Terminal(basisType, basis, term, inLimit)
    qtbot.addWidget(terminal)
    assert(terminal.basisType == basisType)
    assert(terminal.basis == basis)
    assert(terminal.term == term)
    assert(terminal.inLimit == inLimit)
    assert(terminal.toolTip() == tooltip)

  failuresInit = [
    ('test', 'test', 'test', 'test'),
    ([], [], [], []),
    ((), (), (), ()),
    ({}, {}, {}, {}),
    (lambda x: x+1, lambda x: x+1, lambda x: x+1, lambda x: x+1)
  ]

  @pytest.mark.parametrize('basisType, basis, term, inLimit', failuresInit)
  def testInitFailures(self, qtbot, basisType, basis, term, inLimit):
    with pytest.raises(TypeError, match='Unexpected type, expected int not'):
      terminal = Terminal(basisType=basisType)

    with pytest.raises(TypeError, match='Unexpected type, expected int not'):
      terminal = Terminal(basis=basis)

    with pytest.raises(TypeError, match='Unexpected type, expected int not'):
      terminal = Terminal(term=term)

    with pytest.raises(TypeError, match='Unexpected type, expected bool not'):
      terminal = Terminal(inLimit=inLimit)

  #====================================================================
  #                             METHODS
  #==================================================================

  getColors = [
    (Terminal.TYPE_ANY, QColor(169, 169, 169)),
    (Terminal.TYPE_INT, QColor(55, 49, 206)),
    (Terminal.TYPE_FLOAT, QColor(237, 154, 0)),
    (Terminal.TYPE_STRING, QColor(116, 255, 0))
  ]

  @pytest.mark.parametrize('basisType, expectedColor', getColors)
  def testGetColor(self, qtbot, basisType, expectedColor):
    terminal = Terminal(basisType)
    qtbot.addWidget(terminal)
    assert(terminal.getColor() == expectedColor)

  setData = [
    'test',
    'test2',
    1,
    123.3,
    [1, 2, 3],
    { 'data': 'stuff' }
  ]

  @pytest.mark.parametrize('data', setData)
  def testSetData(self, qtbot, data):
    terminal = Terminal()
    qtbot.addWidget(terminal)
    assert(terminal.data == None)
    terminal.setData(data)
    assert(terminal.data == data)

  def testGlobalCenter(self, qtbot):
    terminal = Terminal()
    qtbot.addWidget(terminal)
    assert(terminal.globalCenter() == QPoint(12, 12))

  def testDisconnectedWire(self, qtbot):
    terminal = Terminal()
    qtbot.addWidget(terminal)
    terminal.setData('test')
    assert(terminal.data == 'test')
    terminal.disconnectedWire()
    assert(terminal.data == None)

  def test_addWireConnection(self, qtbot):
    terminal1 = Terminal(term=Terminal.IN_TERM)
    terminal2 = Terminal(term=Terminal.OUT_TERM)
    qtbot.addWidget(terminal1)
    qtbot.addWidget(terminal2)
    terminal1._addWireConnection(None, terminal2)
    terminal2._addWireConnection(None, terminal1)
    assert(len(terminal1.wireConnections) == 1)
    assert(len(terminal2.wireConnections) == 1)
    assert(terminal1.wireConnections[0]['term'] == terminal2)
    assert(terminal2.wireConnections[0]['term'] == terminal1)

  def test_removeLastWireConnection(self, qtbot):
    terminal1 = Terminal(term=Terminal.IN_TERM)
    terminal2 = Terminal(term=Terminal.OUT_TERM)
    qtbot.addWidget(terminal1)
    qtbot.addWidget(terminal2)
    terminal1._addWireConnection(None, terminal2)
    terminal2._addWireConnection(None, terminal1)
    assert(terminal1.wireConnections[0]['term'] == terminal2)
    assert(terminal2.wireConnections[0]['term'] == terminal1)
    terminal1._removeLastWireConnection()
    terminal2._removeLastWireConnection()
    assert(len(terminal1.wireConnections) == 0)
    assert(len(terminal2.wireConnections) == 0)

  def test_removeWireSuccess(self, qtbot):
    terminal1 = Terminal(term=Terminal.IN_TERM)
    terminal2 = Terminal(term=Terminal.OUT_TERM)
    qtbot.addWidget(terminal1)
    qtbot.addWidget(terminal2)
    w = Wire(terminal1, terminal2, GraphEditor())
    terminal1._addWireConnection(w, terminal2)
    terminal2._addWireConnection(w, terminal1)
    assert(len(terminal2.wireConnections) == 1)
    assert(len(terminal1.wireConnections) == 1)
    terminal1._removeWire(0)
    assert(len(terminal2.wireConnections) == 0)
    assert(len(terminal1.wireConnections) == 0)

  def test_removeWireFailure1(self, qtbot):
    terminal = Terminal(term=Terminal.IN_TERM)
    qtbot.addWidget(terminal)
    with pytest.raises(TypeError, match='Unexpected type, expected Integer not:'):
      terminal._removeWire('yes')

  def test_removeWireFailure2(self, qtbot):
    terminal = Terminal(term=Terminal.IN_TERM)
    qtbot.addWidget(terminal)
    with pytest.raises(IndexError, match='Index out of range'):
      terminal._removeWire(4)
    
  @pytest.mark.parametrize('data', setData)
  def testGetData(self, qtbot, data):
    terminal = Terminal()
    qtbot.addWidget(terminal)
    assert(terminal.getData() == None)
    terminal.setData(data)
    assert(terminal.getData() == data)

  @pytest.mark.parametrize('data', setData)
  def testGetData2Terminals(self, qtbot, data):
    terminal1 = Terminal(term=Terminal.IN_TERM)
    terminal2 = Terminal(term=Terminal.OUT_TERM)
    qtbot.addWidget(terminal1)
    qtbot.addWidget(terminal2)
    terminal1._addWireConnection(None, terminal2)
    terminal2._addWireConnection(None, terminal1)
    terminal2.setData(data)
    assert(terminal1.getData() == data)

  testMultiData = [
    ('test1', 'test2'),
    (123, 456),
    (3.14, 2.73),
    ('mixed', { 'data': 1123 })
  ]

  @pytest.mark.parametrize('data1, data2', testMultiData)
  def testGetData3Terminals(self, qtbot, data1, data2):
    terminal1 = Terminal(term=Terminal.IN_TERM)
    terminal2 = Terminal(term=Terminal.OUT_TERM)
    terminal3 = Terminal(term=Terminal.OUT_TERM)
    qtbot.addWidget(terminal1)
    qtbot.addWidget(terminal2)
    qtbot.addWidget(terminal3)
    terminal1._addWireConnection(None, terminal2)
    terminal1._addWireConnection(None, terminal3)
    terminal2._addWireConnection(None, terminal1)
    terminal3._addWireConnection(None, terminal1)
    terminal2.setData(data1)
    terminal3.setData(data2)
    assert(data1 in terminal1.getData())
    assert(data2 in terminal1.getData())

  def testGetData10Terminals(self, qtbot):
    terminals = []
    inTerm = Terminal(term=Terminal.IN_TERM)
    qtbot.addWidget(inTerm)
    for i in range(10):
      t = Terminal(term=Terminal.OUT_TERM)
      terminals.append(t)
      qtbot.addWidget(t)
      t._addWireConnection(None, inTerm)
      inTerm._addWireConnection(None, t)
      t.setData(i)

    for i in range(10):
      assert(i in inTerm.getData())

  def testLimitedInPutWire(self, qtbot):
    graphEditor = GraphEditor()
    scene = graphEditor.scene
    inTerm = Terminal(term=Terminal.IN_TERM, inLimit=True)
    termA = Terminal(term=Terminal.OUT_TERM)
    termB = Terminal(term=Terminal.OUT_TERM)
    qtbot.addWidget(graphEditor)
    qtbot.addWidget(inTerm)
    qtbot.addWidget(termA)
    qtbot.addWidget(termB)
    termA.setData('A')
    termB.setData('B')
    wireA = Wire(termA, inTerm, graphEditor)
    wireB = Wire(termB, inTerm, graphEditor)
    scene.addItem(wireA)
    scene.addItem(wireB)
    inTerm._addWireConnection(wireA, termA)
    assert(len(inTerm.wireConnections) == 1)
    assert(inTerm.getData() == 'A')
    inTerm._addWireConnection(wireB, termB)
    assert(len(inTerm.wireConnections) == 1)
    assert(inTerm.getData() == 'B')

  def testMousePressEvent(self, qtbot):
    terminal = Terminal()

    def sigCap(t):
      assert(t == terminal)

    terminal.terminalClicked.connect(sigCap)
    with qtbot.waitSignal(terminal.terminalClicked):
      qtbot.mousePress(terminal, Qt.LeftButton)