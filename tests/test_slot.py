import pytest

from PyQt5.QtWidgets import QLabel

from NodalPy.parts.slot import GraphNodeSlot
from NodalPy.parts.terminal import Terminal

class TestGraphNodeSlot():
  """
  [x] init
  [x] methods
    [x] inputDataChange (signal)
    [x] outputDataChange (signal)
    [x] getInputData
    [x] setOutputData
  """

  #====================================================================
  #                               INIT
  #====================================================================

  def testInitDefault(self, qtbot):
    slot = GraphNodeSlot()
    qtbot.addWidget(slot)
    assert(slot.input == None)
    assert(slot.output == None)
    assert(slot.widget.text() == QLabel('').text())

  successesInitInput = [
    ({ 'type': Terminal.TYPE_INT, 'basis': Terminal.BASIS_VECTOR }, [Terminal.TYPE_INT, Terminal.BASIS_VECTOR]),
    ({ 'type': Terminal.TYPE_FLOAT, 'basis': Terminal.BASIS_SAMPLER3D }, [Terminal.TYPE_FLOAT, Terminal.BASIS_SAMPLER3D]),
    ({ 'type': Terminal.TYPE_STRING, 'basis': Terminal.BASIS_BASE }, [Terminal.TYPE_STRING, Terminal.BASIS_BASE])
  ]

  @pytest.mark.parametrize('input, expected', successesInitInput)
  def testInitInputs(self, qtbot, input, expected):
    slot = GraphNodeSlot(input)
    qtbot.addWidget(slot)
    assert(slot.input.term == Terminal.IN_TERM)
    assert(slot.input.basisType == expected[0])
    assert(slot.input.basis == expected[1])

  successesInitOutput = [
    ({ 'type': Terminal.TYPE_INT, 'basis': Terminal.BASIS_VECTOR }, [Terminal.TYPE_INT, Terminal.BASIS_VECTOR]),
    ({ 'type': Terminal.TYPE_FLOAT, 'basis': Terminal.BASIS_SAMPLER3D }, [Terminal.TYPE_FLOAT, Terminal.BASIS_SAMPLER3D]),
    ({ 'type': Terminal.TYPE_STRING, 'basis': Terminal.BASIS_BASE }, [Terminal.TYPE_STRING, Terminal.BASIS_BASE])
  ]

  @pytest.mark.parametrize('output, expected', successesInitOutput)
  def testInitOutput(self, qtbot, output, expected):
    slot = GraphNodeSlot(None, output)
    qtbot.addWidget(slot)
    assert(slot.output.term == Terminal.OUT_TERM)
    assert(slot.output.basisType == expected[0])
    assert(slot.output.basis == expected[1])

  failuresInitInputsOutputs = [
    ({}, 'test'),
    (None, 'test'),
    ([], 123),
    (123, []),
    ((), 2.1),
    (1.2, ()),
    (lambda x: x+1, False),
    (True, False)
  ]

  @pytest.mark.parametrize('input, output', failuresInitInputsOutputs)
  def testInitFailuresInputsOutputs(self, qtbot, input, output):
    with pytest.raises(TypeError, match='Unexpected type, expected None or dict got'):
      slot = GraphNodeSlot(input, output)

  failuresInitWidget = [
    True, 'test', 123, 12.2, lambda x: x+1, {}, [], ()
  ]

  @pytest.mark.parametrize('widget', failuresInitWidget)
  def testInitFailuresWidgets(self, qtbot, widget):
    with pytest.raises(TypeError, match='Unexpected type, expected None or QWidget got'):
      slot = GraphNodeSlot(None, None, widget)

  #====================================================================
  #                             METHODS
  #====================================================================

  testGetInputDataData = [
    (123), (12.0), ('test'), ([1, 2, 3])
  ]

  @pytest.mark.parametrize('data', testGetInputDataData)
  def testGetInputData(self, qtbot, data):
    slot = GraphNodeSlot({ 'type': Terminal.TYPE_ANY, 'basis': Terminal.BASIS_BASE })
    qtbot.addWidget(slot)
    slot.input.setData(data)
    assert(slot.getInputData() == data)

  @pytest.mark.parametrize('data', testGetInputDataData)
  def testSetOutputData(self, qtbot, data):
    slot = GraphNodeSlot(None, { 'type': Terminal.TYPE_ANY, 'basis': Terminal.BASIS_BASE })
    qtbot.addWidget(slot)
    assert(slot.output.getData() == None)
    slot.setOutputData(data)
    assert(slot.output.getData() == data)

  @pytest.mark.parametrize('data', testGetInputDataData)
  def testGetInputDataSignal(self, qtbot, data):
    def signalCap(d):
      assert(d == data)

    slot = GraphNodeSlot({ 'type': Terminal.TYPE_ANY, 'basis': Terminal.BASIS_BASE })
    qtbot.addWidget(slot)
    slot.inputDataChange.connect(signalCap)
    with qtbot.waitSignal(slot.inputDataChange):
      slot.input.setData(data)

  @pytest.mark.parametrize('data', testGetInputDataData)
  def testGetOutputDataSignal(self, qtbot, data):
    def signalCap(d):
      assert(d == data)

    slot = GraphNodeSlot(None, { 'type': Terminal.TYPE_ANY, 'basis': Terminal.BASIS_BASE })
    qtbot.addWidget(slot)
    slot.outputDataChange.connect(signalCap)
    with qtbot.waitSignal(slot.outputDataChange):
      slot.output.setData(data)
