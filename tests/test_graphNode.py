import pytest

from PyQt5.QtWidgets import QLabel, QGraphicsProxyWidget, QPushButton, QWidget
from PyQt5.QtCore import QPointF, QPoint, Qt

from NodalPy.graphNode import GraphNode, GraphNodeProxy
from NodalPy.graphEditor import GraphEditor
from NodalPy.parts.terminal import Terminal

def widgeter(widget, value):
  if value == None:
    return widget()
  
  return widget(value)

class TestGraphNode():
  """
  [x] init
  [] methods
    [x] _sceneAdded
    [x] setPos
    [x] addSlot
    [x] addWidget
    [x] _updateSize
    [x] getSlots
    [x] _mapGlobalToScene
  """

  #====================================================================
  #                               INIT
  #====================================================================

  def testInitDefault(self, qtbot):
    node = GraphNode()
    assert(isinstance(node.widget, GraphNodeProxy))

  successesInit = [
    ('test', True),
    ('test2', True),
    ('some other title', False)
  ]

  @pytest.mark.parametrize('title, close', successesInit)
  def testInitTitleSuccesses(self, qtbot, title, close):
    node = GraphNode(title, close)
    assert(node.widget.header_title.text() == title)
    if close:
      assert(node.widget.header_close != None)

    else:
      assert(node.widget.header_close == None)

  failuresTitleInit = [
    (1, 1),
    (2, 2),
    (12.3, 12.3),
    (False, None),
    (True, 'test'),
    ({}, {}),
    ([], []),
    (object(), object()),
    (lambda x: x+1, lambda x: x+1)
  ]

  @pytest.mark.parametrize('title, close', failuresTitleInit)
  def testInitTitleFailures(self, qtbot, title, close):
    with pytest.raises(TypeError, match='title should be a String, not'):
      node = GraphNode(title)

    with pytest.raises(TypeError, match='close should be a Boolean, not'):
      node = GraphNode('test', close)


  #====================================================================
  #                             METHODS
  #====================================================================

  def test_sceneAdded(self, qtbot):
    editor = GraphEditor()
    qtbot.addWidget(editor)

    node = GraphNode()
    editor.addNode(node)
    assert(isinstance(node.proxy, QGraphicsProxyWidget))

  successesPos = [
    QPointF(5, 5),
    QPointF(10, 1),
    QPointF(10, -100),
    QPointF(123, -100)
  ]

  @pytest.mark.parametrize('pos', successesPos)
  def testSetPosSuccesses(self, qtbot, pos):
    editor = GraphEditor()
    qtbot.addWidget(editor)

    node = GraphNode()
    editor.addNode(node)
    assert(node.pos() == QPointF(0, 0))
    node.setPos(pos)
    assert(node.pos() == pos)

  failuresPos = [
    'test',
    ({}, []),
    (1, 2),
    True,
    False,
    None,
    123,
    12.2,
    lambda x: x+1
  ]

  @pytest.mark.parametrize('pos', failuresPos)
  def testSetPosSuccesses(self, qtbot, pos):
    editor = GraphEditor()
    qtbot.addWidget(editor)

    node = GraphNode()
    editor.addNode(node)
    with pytest.raises(TypeError, match='pos needs to be a QPointF, not'):
      node.setPos(pos)

  def testAddSlotDefault(self, qtbot):
    editor = GraphEditor()
    qtbot.addWidget(editor)

    node = GraphNode()
    assert(len(node.widget.slots) == 1)
    node.addSlot()
    editor.addNode(node)
    
    assert(len(node.widget.slots) == 2)
    assert(node.widget.slots[1].input == None)
    assert(node.widget.slots[1].output == None)
    assert(node.widget.slots[1].widget.text() == '')

  def testAddSlotSuccessesWidget(self, qtbot):
    editor = GraphEditor()
    qtbot.addWidget(editor)

    node = GraphNode()
    node.addSlot(None, None, QPushButton('test'))
    editor.addNode(node)

    assert(isinstance(node.widget.slots[1].widget, QPushButton))

  successesAddSlot = [
    (None, { 'type': Terminal.TYPE_ANY }),
    ({ 'type': Terminal.TYPE_FLOAT }, None),
    ({ 'type': Terminal.TYPE_INT }, { 'basis': Terminal.BASIS_SAMPLER2D }),
    ({ 'type': Terminal.TYPE_STRING, 'basis': Terminal.BASIS_VECTOR }, { 'type': Terminal.TYPE_ANY, 'basis': Terminal.BASIS_VECTOR2 })
  ]

  @pytest.mark.parametrize('input, output', successesAddSlot)
  def testAddSlotSuccesses(self, qtbot, input, output):
    T_in = None
    T_out = None
    if input != None:
      it = input['type'] if 'type' in input.keys() != None else 0
      ib = input['basis'] if 'basis' in input.keys() != None else 0
      T_in = Terminal(it, ib, Terminal.IN_TERM)

    if output != None:
      ot = output['type'] if 'type' in output.keys() != None else 0
      ob = output['basis'] if 'basis' in output.keys() != None else 0
      T_out = Terminal(ot, ob, Terminal.OUT_TERM)

    editor = GraphEditor()
    qtbot.addWidget(editor)

    node = GraphNode()
    node.addSlot(input, output)
    editor.addNode(node)

    assert(node.widget.slots[1].input == T_in)
    assert(node.widget.slots[1].output == T_out)

  failuresAddSlot = [
    ('test', 1),
    ((), 'test'),
    ({}, []),
    (123, False),
    (True, lambda x: x+1)
  ]

  @pytest.mark.parametrize('input, output', failuresAddSlot)
  def testAddSlotFailures(self, qtbot, input, output):
    node = GraphNode()
    with pytest.raises(TypeError, match='Unexpected type, expected None or dict got'):
      node.addSlot(input, output)

  @pytest.mark.parametrize('input, output', failuresAddSlot)
  def testAddSlotFailuresWidgets(self, qtbot, input, output):
    node = GraphNode()
    with pytest.raises(TypeError, match='Unexpected type, expected None or QWidget got'):
      node.addSlot(None, None, input)

    with pytest.raises(TypeError, match='Unexpected type, expected None or QWidget got'):
      node.addSlot(None, None, output)

  successesAddWidget = [
    (QLabel, 'test'),
    (QPushButton, 'test'),
    (QWidget, None)
  ]

  @pytest.mark.parametrize('widget, value', successesAddWidget)
  def testAddWidgetSuccesses(self, qtbot, widget, value):
    editor = GraphEditor()
    qtbot.addWidget(editor)

    w = widgeter(widget, value)

    node = GraphNode()
    assert(len(node.widget.slots) == 1)
    node.addWidget(w)
    assert(len(node.widget.slots) == 2)
    assert(isinstance(node.widget.slots[1].widget, widget))

  failuresAddWidget = [
    'test',
    123,
    {},
    True,
    False,
    [],
    object(),
    lambda x: x+1
  ]

  @pytest.mark.parametrize('widget', failuresAddWidget)
  def testAddWidgetFailures(self, qtbot, widget):
    node = GraphNode()

    with pytest.raises(TypeError, match='Unexpected type, expected None or QWidget'):
      node.addWidget(widget)

  def testUpdateSize(self, qtbot):
    node = GraphNode()
    assert(node.background.boundingRect().width() == 1.0)
    assert(node.background.boundingRect().height() == 1.0)

    node.addSlot({ 'type': Terminal.TYPE_ANY })
    qtbot.wait(10)
    
    assert(node.background.boundingRect().width() > 1.0)
    assert(node.background.boundingRect().height() > 1.0)

  def testGetSlots(self, qtbot):
    node = GraphNode()
    assert(len(node.getSlots()) == 1)
    node.addSlot()
    assert(isinstance(node.getSlots(), list))
    assert(len(node.getSlots()) == 2)

  successes_mapGlobalToScene = [
    QPoint(0, 0),
    QPoint(1000, 0),
    QPoint(10, 35),
  ]

  @pytest.mark.parametrize('point', successes_mapGlobalToScene)
  def test_mapGlobalToSceneSuccesses(self, qtbot, point):
    editor = GraphEditor()
    qtbot.addWidget(editor)
    editor.move(100, 100)

    node = GraphNode()
    editor.addNode(node)
    assert(node._mapGlobalToScene(point) == point - QPoint(100, 100))

  failures_mapGlobalToScene = [
    None,
    'test',
    123,
    [123, 1],
    ('set', 'asd'),
    {},
    False,
    True,
    123.3,
    lambda x: x+1
  ]

  @pytest.mark.parametrize('point', failures_mapGlobalToScene)
  def test_mapGlobalToSceneFailures(self, qtbot, point):
    editor = GraphEditor()
    qtbot.addWidget(editor)

    node = GraphNode()
    editor.addNode(node)
    with pytest.raises(TypeError, match='Unexpected type, expected QPoint not'):
      node._mapGlobalToScene(point)


class TestGraphNodeProxy():
  """
  [x] init
  [x] methods
    [x] _droppedLine
    [x] _updateSize
    [x] addSlot
    [] close
    [x] _isCyclic
  """

  #====================================================================
  #                               INIT
  #====================================================================

  def testInitDefault(self, qtbot):
    proxy = GraphNodeProxy()
    qtbot.addWidget(proxy)
    assert(proxy.control == None)
    assert(proxy.header_title.text() == 'GraphNode')

  successesInit = [
    'test',
    'title',
    'titles',
    'something'
  ]

  @pytest.mark.parametrize('title', successesInit)
  def testInitSuccesses(self, qtbot, title):
    proxy = GraphNodeProxy(title)
    qtbot.addWidget(proxy)
    assert(proxy.control == None)
    assert(proxy.header_title.text() == title)

  def testInitControl(self, qtbot):
    node = GraphNode()
    proxy = GraphNodeProxy(control=node)
    qtbot.addWidget(proxy)
    assert(proxy.control == node)

  failuresInit = [
    ({}, 'test'),
    ([], 123),
    (123, 12.3),
    (False, True),
    (True, False),
    (lambda x: x+1, []),
    (1.2, {}),
    (object(), object()),
    (None, lambda x: x+1)
  ]

  @pytest.mark.parametrize('title, control', failuresInit)
  def testInitFailures(self, qtbot, title, control):
    with pytest.raises(TypeError, match='title should be a String, not'):
      proxy = GraphNodeProxy(title)

    with pytest.raises(TypeError, match='Unexpected type, expected GraphNode or None not'):
      proxy = GraphNodeProxy(control=control)


  #====================================================================
  #                             METHODS
  #====================================================================

  def test_droppedLine(self, qtbot):
    editor = GraphEditor()
    qtbot.addWidget(editor)

    node = GraphNode()
    node.addSlot({ 'type': Terminal.TYPE_ANY })

    editor.addNode(node)

    pos = node.widget.slots[1].input.globalCenter()
    proxy = node.widget
    assert(proxy._droppedLine(QPoint(0, 0)) == None)
    assert(proxy._droppedLine(pos) == node.widget.slots[1].input)
    assert(proxy._droppedLine(pos + QPoint(6, 6)) == node.widget.slots[1].input)
    assert(proxy._droppedLine(pos + QPoint(12, 12)) == node.widget.slots[1].input)
    assert(proxy._droppedLine(pos + QPoint(26, 26)) == None)

  def test_updateSize(self, qtbot):
    editor = GraphEditor()
    qtbot.addWidget(editor)

    node = GraphNode()
    editor.addNode(node)

    proxy = node.widget

    assert(node.background.boundingRect().width() == 1.0)
    assert(node.background.boundingRect().height() == 1.0)

    proxy.addSlot({ 'type': Terminal.TYPE_ANY }, None, QLabel('test'))
    proxy.slots[1].widget.setText('123123123123123123123')
    qtbot.wait(10)
    proxy._updateSize(None)

    assert(node.background.boundingRect().width() > 1.0)
    assert(node.background.boundingRect().height() > 1.0)

  def testAddSlotDefault(self, qtbot):
    editor = GraphEditor()
    qtbot.addWidget(editor)

    node = GraphNode()
    proxy = node.widget
    assert(len(proxy.slots) == 1)
    node.addSlot()
    editor.addNode(node)
    
    assert(len(proxy.slots) == 2)
    assert(proxy.slots[1].input == None)
    assert(proxy.slots[1].output == None)
    assert(proxy.slots[1].widget.text() == '')

  def testAddSlotSuccessesWidget(self, qtbot):
    editor = GraphEditor()
    qtbot.addWidget(editor)

    node = GraphNode()
    proxy = node.widget
    node.addSlot(None, None, QPushButton('test'))
    editor.addNode(node)

    assert(isinstance(proxy.slots[1].widget, QPushButton))

  successesAddSlot = [
    (None, { 'type': Terminal.TYPE_ANY }),
    ({ 'type': Terminal.TYPE_FLOAT }, None),
    ({ 'type': Terminal.TYPE_INT }, { 'basis': Terminal.BASIS_SAMPLER2D }),
    ({ 'type': Terminal.TYPE_STRING, 'basis': Terminal.BASIS_VECTOR }, { 'type': Terminal.TYPE_ANY, 'basis': Terminal.BASIS_VECTOR2 })
  ]

  @pytest.mark.parametrize('input, output', successesAddSlot)
  def testAddSlotSuccesses(self, qtbot, input, output):
    T_in = None
    T_out = None
    if input != None:
      it = input['type'] if 'type' in input.keys() != None else 0
      ib = input['basis'] if 'basis' in input.keys() != None else 0
      T_in = Terminal(it, ib, Terminal.IN_TERM)

    if output != None:
      ot = output['type'] if 'type' in output.keys() != None else 0
      ob = output['basis'] if 'basis' in output.keys() != None else 0
      T_out = Terminal(ot, ob, Terminal.OUT_TERM)

    editor = GraphEditor()
    qtbot.addWidget(editor)

    node = GraphNode()
    proxy = node.widget
    proxy.addSlot(input, output)
    editor.addNode(node)

    assert(proxy.slots[1].input == T_in)
    assert(proxy.slots[1].output == T_out)

  failuresAddSlot = [
    ('test', 1),
    ((), 'test'),
    ({}, []),
    (123, False),
    (True, lambda x: x+1)
  ]

  @pytest.mark.parametrize('input, output', failuresAddSlot)
  def testAddSlotFailures(self, qtbot, input, output):
    node = GraphNode()
    proxy = node.widget
    with pytest.raises(TypeError, match='Unexpected type, expected None or dict got'):
      proxy.addSlot(input, output)

  @pytest.mark.parametrize('input, output', failuresAddSlot)
  def testAddSlotFailuresWidgets(self, qtbot, input, output):
    node = GraphNode()
    proxy = node.widget
    with pytest.raises(TypeError, match='Unexpected type, expected None or QWidget got'):
      proxy.addSlot(None, None, input)

    with pytest.raises(TypeError, match='Unexpected type, expected None or QWidget got'):
      proxy.addSlot(None, None, output)

  def testClose(self, qtbot):
    node = GraphNode()
    editor = GraphEditor()
    qtbot.addWidget(editor)
    editor.addNode(node)

    def sigCap():
      assert(True)

    node.widget.closed.connect(sigCap)

    assert(node.background in editor.scene.items())
    assert(node.proxy in editor.scene.items())

    with qtbot.waitSignal(node.widget.closed):
      qtbot.mousePress(node.widget.header_close, Qt.LeftButton)

    assert(node.background not in editor.scene.items())
    assert(node.proxy not in editor.scene.items())

  def testIsCyclic(self, qtbot):
    editor = GraphEditor()
    qtbot.addWidget(editor)

    node1 = GraphNode()
    proxy1 = node1.widget
    proxy1.addSlot({ 'type': Terminal.TYPE_ANY }, { 'type': Terminal.TYPE_ANY })

    node2 = GraphNode()
    proxy2 = node2.widget
    proxy2.addSlot({ 'type': Terminal.TYPE_ANY }, { 'type': Terminal.TYPE_ANY })
    editor.addNode(node1)
    editor.addNode(node2)
    assert(not proxy1._isCyclic(None, [], []))
    assert(not proxy2._isCyclic(None, [], []))

    proxy1.slots[1].output._addWireConnection(None, proxy2.slots[1].input)
    proxy2.slots[1].input._addWireConnection(None, proxy1.slots[1].output)
    assert(not proxy1._isCyclic(None, [], []))
    assert(not proxy2._isCyclic(None, [], []))

    proxy1.slots[1].input._addWireConnection(None, proxy2.slots[1].output)
    proxy2.slots[1].output._addWireConnection(None, proxy1.slots[1].input)
    assert(proxy1._isCyclic(None, [], []))
    assert(proxy2._isCyclic(None, [], []))