import pytest

from PyQt5.QtCore import QRectF, QPointF

from NodalPy.parts.nodeBackground import Background
from NodalPy.graphNode import GraphNode
from NodalPy.graphEditor import GraphEditor

class EventHelper():

  def __init__(self, x, y):
    self.x = x
    self.y = y

  def pos(self):
    return QPointF(self.x, self.y)

  def scenePos(self):
    return QPointF(self.x, self.y)

class TestBackground():
  """
  [x] init
  [] methods
    [x] mousePressEvent
    [x] mouseReleaseEvent
    [x] mouseMoveEvent
  """

  #====================================================================
  #                               INIT
  #====================================================================

  def testInit(self, qtbot):
    background = Background()
    qtbot.addWidget(background)
    assert(background.rect() == QRectF(32, 15, 0, 0))

  #====================================================================
  #                             METHODS
  #====================================================================

  def testMousePressEvent(self, qtbot):
    background = Background()
    qtbot.addWidget(background)
    assert(not background.isDragging)
    assert(background.mousePos == None)
    background.mousePressEvent(EventHelper(12, 12))
    assert(background.isDragging)
    assert(background.mousePos != None)

  def testMouseMoveEvent(self, qtbot):
    node = GraphNode()
    editor = GraphEditor()
    qtbot.addWidget(editor)
    editor.addNode(node)
    background = node.background
    background.mousePressEvent(EventHelper(0, 0))

    assert(background.pos() == QPointF())
    background.mouseMoveEvent(EventHelper(5, 5))
    assert(background.pos() == QPointF(5.0, 5.0))

  def testMouseReleaseEvent(self, qtbot):
    background = Background()
    qtbot.addWidget(background)
    assert(not background.isDragging)
    background.mousePressEvent(EventHelper(10, 10))
    assert(background.isDragging)
    background.mouseReleaseEvent(None)
    assert(not background.isDragging)