import pytest

from PyQt5.QtCore import QRectF, QPoint, QPointF, Qt
from PyQt5.QtGui import QWheelEvent

from NodalPy.graphEditor import GraphEditor
from NodalPy.graphNode import GraphNode

class QWheelEventHelper(QWheelEvent):

  def __init__(self, i):
    super().__init__(QPointF(0, 0), QPointF(0, 0), QPoint(i, i), QPoint(i*15, i*15), i, Qt.Vertical, Qt.NoButton, Qt.NoModifier)

class TestGraphEditor():
  """
  [x] init
  [x] methods
    [x] addNode
    [x] wheelEvent
  """

  #====================================================================
  #                               INIT
  #====================================================================

  def testDefaultInit(self, qtbot):
    graphEditor = GraphEditor()
    qtbot.addWidget(graphEditor)
    assert(graphEditor.scene.sceneRect() == QRectF(-2000, -2000, 4000, 4000))

  successes = [
    (0, 0),
    (1, 1),
    (0, 1),
    (1, 0),
    (100, 100),
    (2000, 500),
    (4000, 4000),
    (4000, 2000)
  ]

  @pytest.mark.parametrize('w, h', successes)
  def testSuccessInits(self, qtbot, w, h):
    graphEditor = GraphEditor(w, h)
    qtbot.addWidget(graphEditor)
    assert(graphEditor.scene.sceneRect() == QRectF(-w/2, -h/2, w, h))

  failures = [
    (-1, -1, ValueError, 'Out of range'),
    (0, -1, ValueError, 'Out of range'),
    (-1, 0, ValueError, 'Out of range'),
    (-12, -12, ValueError, 'Out of range'),
    (-12, 2000, ValueError, 'Out of range'),
    ('test', 'test', TypeError, 'Expected integer not'),
    ('test', 4, TypeError, 'Expected integer not'),
    (3, 'test', TypeError, 'Expected integer not'),
    ({}, [], TypeError, 'Expected integer not'),
    (2, [], TypeError, 'Expected integer not'),
    ({}, 5, TypeError, 'Expected integer not')
  ]

  @pytest.mark.parametrize('w, h , error, match', failures)
  def testFailureInits(self, qtbot, w, h, error, match):
    with pytest.raises(error, match=match):
      graphEditor = GraphEditor(w, h)

  #====================================================================
  #                             METHODS
  #====================================================================

  def testAddNodeSuccess(self, qtbot):
    graphEditor = GraphEditor()
    qtbot.addWidget(graphEditor)
    node = GraphNode()
    assert(len(graphEditor.nodes) == 0)
    graphEditor.addNode(node)
    assert(len(graphEditor.nodes) == 1)

  def testAddNodeFailure(self, qtbot):
    graphEditor = GraphEditor()
    qtbot.addWidget(graphEditor)
    with pytest.raises(TypeError, match='Expected GraphNode not'):
      graphEditor.addNode('test')

  events = [
    (QWheelEventHelper(1), 1, 1.1),
    (QWheelEventHelper(1), 2, 1.2),
    (QWheelEventHelper(-1), 1, 0.9),
    (QWheelEventHelper(-1), 2, 0.8),
    (QWheelEventHelper(-1), 10, 0.5),
    (QWheelEventHelper(1), 50, 5.0),
  ]

  @pytest.mark.parametrize('event, repeat, result', events)
  def testWheelEvent(self, qtbot, event, repeat, result):
    graphEditor = GraphEditor()
    qtbot.addWidget(graphEditor)
    assert(graphEditor.viewportTransform().m11() == 1.0)
    assert(graphEditor.viewportTransform().m22() == 1.0)
    for i in range(repeat):
      graphEditor.wheelEvent(event)

    assert(graphEditor.viewportTransform().m11() == pytest.approx(result))
    assert(graphEditor.viewportTransform().m22() == pytest.approx(result))