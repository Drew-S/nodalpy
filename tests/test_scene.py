import pytest

from PyQt5.QtCore import QRectF, QPointF

from NodalPy.parts.scene import GraphEditorScene
from NodalPy.graphEditor import GraphEditor
from NodalPy.graphNode import GraphNode
from NodalPy.parts.terminal import Terminal, Wire

class eventHelper():

  def __init__(self, x=0, y=0):
    self.x = x
    self.y = y

  def scenePos(self):
    return QPointF(self.x, self.y)

class TestGraphEditorScene():
  """
  [x] init
  [] methods
    [x] mouseReleaseEvent
    [x] _beginConnectTerminal
    [] mouseMoveEvent
  """

  #====================================================================
  #                               INIT
  #====================================================================

  def testInitDefault(self, qtbot):
    scene = GraphEditorScene()
    qtbot.addWidget(scene)
    assert(scene.sceneRect() == QRectF(-2000, -2000, 4000, 4000))

  successesInit = [
    (1200, 1200),
    (400, 400),
    (800, 300),
    (123, 456),
    (0, 0)
  ]

  @pytest.mark.parametrize('w, h', successesInit)
  def testInitSuccesses(self, qtbot, w, h):
    scene = GraphEditorScene(w, h)
    qtbot.addWidget(scene)
    assert(scene.sceneRect() == QRectF(-w/2, -h/2, w, h))

  #====================================================================
  #                              METHODS
  #====================================================================

  def test_beginConnectTerminalInTerm(self, qtbot):
    editor = GraphEditor()
    qtbot.addWidget(editor)
    scene = editor.scene

    scene._beginConnectTerminal(Terminal())
    assert(isinstance(scene.currentLine, Wire))

  def test_beginConnectTerminalOutTermNoConnections(self, qtbot):
    editor = GraphEditor()
    qtbot.addWidget(editor)
    scene = editor.scene

    scene._beginConnectTerminal(Terminal(term=Terminal.OUT_TERM))
    assert(isinstance(scene.currentLine, Wire))

  def test_beginConnectTerminalOutTermConnections(self, qtbot):
    editor = GraphEditor()
    qtbot.addWidget(editor)
    scene = editor.scene

    term = Terminal()
    term2 = Terminal(term=Terminal.OUT_TERM)
    wire = Wire(term, term2, editor)
    term2._addWireConnection(wire, term)
    term._addWireConnection(wire, term2)

    scene._beginConnectTerminal(term)
    assert(scene.currentLine == term2.wireConnections[0]['line'])

  def testMouseReleaseEvent1(self, qtbot):
    node1 = GraphNode()
    node1.addSlot({ 'type': 0 }, { 'type': 0 })
    node2 = GraphNode()
    node2.addSlot({ 'type': 0 }, { 'type': 0 })

    editor = GraphEditor()

    editor.addNode(node1)
    editor.addNode(node2)

    qtbot.addWidget(editor)
    scene = editor.scene

    scene._beginConnectTerminal(node1.widget.slots[1].input)

    node2.setPos(QPointF(500, 500))
    pos = node2.widget.slots[1].output.globalCenter()
    assert(scene.currentLine != None)

    scene.mouseReleaseEvent(eventHelper(pos.x(), pos.y()))
    assert(scene.currentLine == None)

  def testMouseReleaseEvent2(self, qtbot):
    node1 = GraphNode()
    node1.addSlot({ 'type': 0 }, { 'type': 0 })
    node2 = GraphNode()
    node2.addSlot({ 'type': 0 }, { 'type': 0 })

    editor = GraphEditor()

    editor.addNode(node1)
    editor.addNode(node2)

    qtbot.addWidget(editor)
    scene = editor.scene

    scene._beginConnectTerminal(node1.widget.slots[1].input)

    node2.setPos(QPointF(500, 500))
    pos = node2.widget.slots[1].output.globalCenter()
    assert(scene.currentLine != None)

    scene.mouseReleaseEvent(eventHelper(pos.x(), pos.y()))
    assert(scene.currentLine == None)

    scene._beginConnectTerminal(node1.widget.slots[1].output)
    assert(scene.currentLine != None)

    pos = node2.widget.slots[1].input.globalCenter()

    scene.mouseReleaseEvent(eventHelper(pos.x(), pos.y()))

  def testMouseReleaseEvent3(self, qtbot):
    node = GraphNode()
    node.addSlot({ 'type': 0 }, { 'type': 0 })

    editor = GraphEditor()

    editor.addNode(node)

    qtbot.addWidget(editor)
    scene = editor.scene

    scene._beginConnectTerminal(node.widget.slots[1].output)

    pos = node.widget.slots[1].output.globalCenter()
    assert(scene.currentLine != None)

    scene.mouseReleaseEvent(eventHelper(pos.x(), pos.y()))
    assert(scene.currentLine == None)

  def testMouseMoveEvent(self, qtbot):
    term = Terminal()
    editor = GraphEditor()
    qtbot.addWidget(editor)
    scene = editor.scene

    scene._beginConnectTerminal(term)
    assert(isinstance(scene.currentLine, Wire))
    scene.mouseMoveEvent(eventHelper())
    assert(isinstance(scene.currentLine, Wire))